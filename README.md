# Random Anime Character

-> Gives you the greatest info of all time. Anime characters.

#### How?
The package scraps the contents of myanimelists to find random characters.

#### Usage:

```js
const randomChar = require('random-anime-character');
```
Since this is an asynchronous package, it returns a promise. You can solve this promise using .then()
```js
const myCharacter = randomChar().then((character) => console.log(character))
```
or with `await` keyword.
```js
const myCharacter = await randomChar()
```

The promise returns an Object of this type:
```ts
{
  title: string, // Title of the anime this character is from.
  image: string, // Image URL (MyAnimeList)
  tags: string[], // Character name as an array. ['foo', 'bar', 'baz'] => "Foo Bar Baz
}
```